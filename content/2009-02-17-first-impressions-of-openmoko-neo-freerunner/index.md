+++
title = "First impressions of Openmoko Neo FreeRunner"
aliases = ["2009/02/first-impressions-of-openmoko-neo.html"]
date = "2009-02-17T00:38:00Z"
[taxonomies]
tags = ["Android", "FSO", "gta02", "hardware", "koolu", "Openmoko", "Qt Extended", "Qtopia", "SHR"]
categories = ["hardware", "impressions"]
authors = ["peter"]
+++
Recently I bought a defected FreeRunner from eBay, the articles description just said, that it had stopped starting up some days ago. I wouldn't have bought it, if it wouldn't have been quite cheap: I won the auction and had to pay ~53 euros including shipping&mdash;I had seen another defected FreeRunner with a similar defect priced far higher after all.
<!-- more --> 

I was (of course) excited to get it. When it arrived one day after my G1, I had to take it out of its box immediately. Turned out that it really didn't start. So I connected it to the wall charger, in order to charge the battery. Didn't start, still. Finally, I wired one of my Motorola batteries to it, and guess what: I got a weird looking image on the screen. 

So I thought: Maybe something happened while flashing the device and flashing failed. I asked for help at #openmoko on freenode, and i was told to try to enter the NOR boot loader. So I pressed and held the AUX button before the power button, but: From that what I saw on the screen, i had to consider, that even the NOR flash boot loader was broken and a debug board necessary.

The next morning I decided to open the upper part of the FreeRunner, just to have a look at the JTAG connector. And what did I see? The AUX button was broken. Well, actually it still is, as I don't have a soldering iron&mdash;but i will have one pretty soon. 

Last night I've then been playing with my FreeRunner, had a look at an older version of Qtopia/Qt Extended (which was actually installed on flash), played with SHR unstable, FSO stable and Android (one build from panicking, and now the koolu beta3). 

{{ gallery() }}

I will write more about my experiences with these distributions soon&mdash;currently I don't have the time to do so. 

(The real reason for this post is to show my happiness that I was able to find the (and maybe solve) problem.)


