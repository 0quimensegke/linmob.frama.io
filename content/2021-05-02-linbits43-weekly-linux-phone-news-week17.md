+++
title = "LinBits 43: Weekly Linux Phone news / media roundup (week 17)"
aliases = ["2021/05/02/linbits42-weekly-linux-phone-news-week17.html"]
date = "2021-05-02T20:59:00Z"
updated = "2021-05-04T05:59:00Z"
[taxonomies]
authors = ["peter"]
tags = ["PINE64", "PinePhone", "Purism", "Librem 5", "postmarketOS", "Phosh", "Plasma Mobile", "JingOS", "OpenSUSE", "Nemo Mobile", "LINMOBapps",]
categories = ["weekly update"]
[extra]
update_note = "Added 'Begining with nemomobile', which I had been unable to find again before initial publication."
+++

_It's sunday. Now what happened since last sunday?_

Further delays with Librem 5 shipping, a notable improvement for using Desktop Apps on phones, and more!<!-- more --> _Commentary in italics._


### Software development and releases
* [Phosh 0.10.2](https://social.librem.one/@agx/106149354889259688) has been released, as the number indicates it's mostly a bugfix release. Yet it brings new features, too:  [Full release notes](https://source.puri.sm/Librem5/phosh/-/releases/v0.10.2).
* [Fluffychat 0.30.0](https://fluffychat.im/de/changelog.html) It's now installable (e.g. via [Flathub](https://flathub.org/apps/details/im.fluffychat.Fluffychat)) and starts on the PinePhone, but logging in did not work for me, yet.
* OpenSUSE [has seen another release](https://twitter.com/hadrianweb/status/1388189569615638530), [Images](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/?C=N;O=D), [Changelog](https://gitlab.com/slem.os/slem.os/-/blob/master/CHANGELOG.md). _They now use BTRFS as main file system. OpenSUSE have also packaged [Telegrand](https://twitter.com/hadrianweb/status/1388561384942747648), a new GTK Telegram client, and an app for [vcf import](https://twitter.com/hadrianweb/status/1388255500807122950)._

### Worth noting
* [Nheko 0.8.2](https://github.com/Nheko-Reborn/nheko/releases/tag/v0.8.2) was relesed the week before, but I only tried it this week and must recommend it: Many bugs have been fixed, it can be used on the PinePhone yet. _It's not totally crash free yet, but definitely a lot better!_
* Manjaro Phosh has seen notable [improvements with regard to battery life](https://twitter.com/ManjaroLinux/status/1388400293818347520).

### Worth reading 

#### Software corner
* Linux Smartphones: [Virtual mouse app for Linux phones makes desktop apps easier to use](https://linuxsmartphones.com/virtual-mouse-app-for-linux-phones-makes-desktop-apps-easier-to-use/). _This is a great development!_
* Plasma Mobile Team: [Plasma Mobile: More Applications and an Improved Homescreen](https://www.plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/). _Impressive updates! Great to see work on a mail client._
  * Linux Smartphones: [Plasma Mobile picks up new apps, app updates, and the beginnings of multiple home screens](https://linuxsmartphones.com/plasma-mobile-picks-up-new-apps-app-updates-and-the-beginnings-of-multiple-home-screens/). _Brad's take._
* UBports: [Ubuntu Touch Q&A 99 Blog](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-q-a-99-3752). _The written and audio version of last weeks video recording._
* Jozef Mlich: [Begining with nemomobile](https://blog.mlich.cz/2021/04/begining-with-nemomobile/). _Please join Jozef and help Nemo Mobile out, it has so much potential!_


#### Hardware corner
* Purism: [The Ball and Supply Chain](https://puri.sm/posts/the-ball-and-supply-chain/). _If you have not received a shipping mail yet, be ready to wait until at least the fourth quarter._
  * Linux Smartphones: [Librem 5 smartphone gets a price hike and shipping delay in response to global component shortages](https://linuxsmartphones.com/librem-15-smartphone-gets-a-price-hike-and-shipping-delay-in-response-to-global-component-shortages/). _Brad's take._
 

### Worth listening
* postmarketOS podcast: [#5 Alpineconf, and interview with proycon from sxmo](https://cast.postmarketos.org/episode/05-Interview-with-proycon-from-sxmo/). _Great show: Listen in for information about Sxmo and get excited for Alpineconf!_

### Worth watching
* Sk4zZi0uS: [PinePhone Challenge 4 week Update (It ain't fast but its good enough) - 2021-04-30](https://www.youtube.com/watch?v=myR_mf1vopI). _I love his enthusiasm!_

#### Software corner
* mutantC: [Using CorePad in postmarketOS, Nexus 5x](https://www.youtube.com/watch?v=E1Nz5ZzP4Hg) . _Another CuboCore app demoed._
* CalcProgrammer1: [Touchpad Emulator - A Virtual Mouse for Linux Phones (PinePhone)](https://www.youtube.com/watch?v=C4-4rrPS0a0). _This is really cool!_


#### Tutorial corner
* PINE64: [PinePhone Beta Edition Quickstart Guide](https://www.youtube.com/watch?v=6TKpJsXDDng). _Great video by PizzaLovingNerd, and thanks for the shoutout!_
* Martijn Braam: [How to insert SIM and SD into a PinePhone: The directors cut](https://www.youtube.com/watch?v=SiQSJSrPqd8). _This is one of my favourite PinePhone videos ever!_
* Privacy & Tech Tips: [Firejail To Increase Privacy On Linux (Pinephone/Pinetab Shown But Works For All Desktops/Laptops)](https://odysee.com/@RTP:9/firejail-to-increase-privacy-on-linux:b). _Firejail is quite a nice program, I must say!_

#### Jing corner
* JingOS: [Developer Update \| Gimp, Firefox and Libre Office test on JingOS ARM](https://www.youtube.com/watch?v=7Qvp_QtaCDg). _Just because you can, does not mean you should. Also, Touchpad Emulator (see above) would have been quite helpful here. It's also noteworthy that we know now that this phone has an octa core CPU and 4GB RAM._
* Geotechland: [The Best Linux Tablet Experience?](https://odysee.com/@geotechland:6/the-best-linux-tablet-experience:6) _Nice video!_

#### Unboxing Corner
* HyperVegan: [Pine64 Pinephone \| Convergence Package Unboxing](https://odysee.com/@HyperVegan:2/20210502_Pine64_Pinephone_Convergence_Unboxing_720p:0).
* Matt Bulfinch: [Ripping open the PinePhone 64...](https://www.youtube.com/watch?v=SmERlhSM-I4).
* Paulo Kretcheu: [Curso GNU Linux - Lab GNU #07 - Unboxing PinePhone.](https://www.youtube.com/watch?v=0j4q4RGPkSg). _Really in depth, and in spanish._


### Stuff I did

#### Content
I wrote two blog posts, one announcing the [overhaul of this blog](https://linmob.net/bringing-back-old-content/), and another discussing [the question of the viability of this new Linux Phone ecosystem](https://linmob.net/will-linux-phones-stay-around-this-time/).


#### LINMOBapps
I added two apps to LINMOBapps, so that we have a total of 259 apps right now: Added DayKountdown and OptiImage. [See here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)!
